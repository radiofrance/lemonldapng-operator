package main

import (
	"fmt"

	"github.com/spf13/cobra"
)

var (
	version = "dev"
	commit  = "none"
	date    = "unknown"
	builtBy = "unknown"
)

// versionCmd represents the version command.
var versionCmd = &cobra.Command{
	Use:   "version",
	Short: "Print version and build information",
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Printf("%s v%s\n", cmd.Root().Name(), version) //nolint:forbidigo
		fmt.Printf("commit %s\n", commit)                  //nolint:forbidigo
		fmt.Printf("built at %s by %s\n", date, builtBy)   //nolint:forbidigo
	},
}

func init() {
	rootCmd.AddCommand(versionCmd)
}
