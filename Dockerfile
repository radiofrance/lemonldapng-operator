FROM golang:1.21.3@sha256:24a09375a6216764a3eda6a25490a88ac178b5fcb9511d59d0da5ebf9e496474 as builder

ENV CGO_ENABLED=0
ENV GOARCH=amd64
ENV GOOS=linux

COPY . /src

RUN cd /src \
    && go build -a -installsuffix cgo -ldflags '-extldflags "-static"' -o lemonldapng-controller ./cmd

FROM debian:bookworm

RUN set -x \
    && apt-get update -y \
    && apt-get install -y ca-certificates \
    && rm -rf /var/lib/apt/lists/* \
    && groupadd --gid 1664 gopher \
    && useradd --comment 'Go user' --uid 1664 --gid 1664 --shell /bin/sh gopher

COPY --from=builder /src/lemonldapng-controller /usr/local/bin/lemonldapng-controller

USER gopher

ENTRYPOINT [ "/usr/local/bin/lemonldapng-controller" ]
