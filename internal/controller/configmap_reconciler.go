//nolint:gosec // sha1.Sum is only used to generate relative uniq key only (not for security)
package controller

import (
	"context"
	"crypto/sha1"
	"encoding/base64"
	"encoding/json"
	"fmt"

	"github.com/sirupsen/logrus"
	lemonldap "gitlab.com/radiofrance/lemonldapng-operator/internal/lemonldapng/config"
	appsv1 "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/types"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
)

// ReconcileConfigmap reconciles the target configmap with the latest LemonLDAP::NG configuration.
func (ctrl *Controller) ReconcileConfigmap(ctx context.Context, request reconcile.Request) (reconcile.Result, error) {
	// NOTE: only target configmap can be reconciled
	if request.NamespacedName != (types.NamespacedName{
		Namespace: ctrl.config.LemonHandler.Configmap.Namespace,
		Name:      ctrl.config.LemonHandler.Configmap.Name,
	}) {
		return reconcile.Result{}, nil
	}

	raw, err := ctrl.generateConfiguration(ctrl.vhosts)
	if err != nil {
		return reconcile.Result{}, err
	}

	var configmap corev1.ConfigMap

	err = ctrl.client.Get(ctx, request.NamespacedName, &configmap)
	if err != nil && !errors.IsNotFound(err) {
		return reconcile.Result{}, fmt.Errorf("retrieving configmap failed: %w", err)
	}

	configmap.Data = map[string]string{
		ctrl.config.LemonHandler.Configmap.Key: string(raw),
	}

	// If configmap doesn't exist, we should create it
	if errors.IsNotFound(err) {
		configmap.Name = request.Name
		configmap.Namespace = request.Namespace
		if err := ctrl.client.Create(ctx, &configmap); err != nil {
			return reconcile.Result{}, fmt.Errorf("creating configmap failed: %w", err)
		}
	} else {
		if err := ctrl.client.Update(ctx, &configmap); err != nil {
			return reconcile.Result{}, fmt.Errorf("updating configmap failed: %w", err)
		}
	}

	if !ctrl.config.LemonHandler.Deployment.ForceReload {
		return reconcile.Result{}, nil
	}

	if err := ctrl.restartHandlerDeployment(ctx, raw); err != nil {
		logrus.Errorf("can't update Handler deployment: %v", err)
		return reconcile.Result{}, fmt.Errorf("can't update Handler deployment: %w", err)
	}
	return reconcile.Result{}, nil
}

// generateConfiguration generates a new version of the LemonLDAP::NG handler configuration based on the
// given ingress configuration.
func (ctrl *Controller) generateConfiguration(vhosts lemonldap.VHosts) ([]byte, error) {
	content, err := ctrl.watcher.Content()
	if err != nil {
		return nil, fmt.Errorf("invalid base configuration: %w", err)
	}

	base, err := lemonldap.NewConfigFromJSON(content)
	if err != nil {
		return nil, fmt.Errorf("invalid base configuration: %w", err)
	}

	base.VHosts = base.VHosts.Overwrite(vhosts)
	raw, err := json.MarshalIndent(base, "", "  ")
	if err != nil {
		return nil, fmt.Errorf("configuration marshaling failed: %w", err)
	}

	return raw, nil
}

// restartHandlerDeployment restarts the LemonLDAP::NG handler when a new configuration is updated.
func (ctrl *Controller) restartHandlerDeployment(ctx context.Context, raw []byte) error {
	hash := sha1.Sum(raw) //nolint:gosec // sha1.Sum is only used to generate relative uniq key only (not for security)
	hashStr := base64.URLEncoding.EncodeToString(hash[:])

	deployment := &appsv1.Deployment{}
	key := types.NamespacedName{
		Namespace: ctrl.config.LemonHandler.Deployment.Namespace,
		Name:      ctrl.config.LemonHandler.Deployment.Name,
	}

	if err := ctrl.client.Get(ctx, key, deployment); err != nil {
		return fmt.Errorf("can't get lemonldap handler's deployment: %w", err)
	}

	if deployment.Spec.Template.ObjectMeta.Annotations == nil {
		deployment.Spec.Template.ObjectMeta.Annotations = map[string]string{}
	} else if deployment.Spec.Template.ObjectMeta.Annotations["baseconfhash"] == hashStr {
		logrus.Infof("'baseconfhash' annotation is unchanged, lemon deployment will not be rollout")
		return nil
	}

	deployment.Spec.Template.ObjectMeta.Annotations["baseconfhash"] = hashStr

	logrus.Debugf("'baseconfhash' hash has changed, lemon deployment will not be rollout")

	if err := ctrl.client.Update(ctx, deployment); err != nil {
		return fmt.Errorf("can't update lemonldap handler's deployment: %w", err)
	}

	return nil
}
