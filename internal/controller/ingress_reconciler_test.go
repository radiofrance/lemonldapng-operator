//nolint:testpackage,forcetypeassert
package controller

import (
	"context"
	"testing"

	"github.com/stretchr/testify/suite"
	lemonldap "gitlab.com/radiofrance/lemonldapng-operator/internal/lemonldapng/config"
	networkingv1 "k8s.io/api/networking/v1"
	"k8s.io/apimachinery/pkg/api/errors"
)

type IngressReconcilerTestSuite struct {
	ControllerTestSuiteBase
}

func (s *IngressReconcilerTestSuite) TestNotFound() {
	result, err := s.controller.ReconcileIngress(context.Background(), requestIngress)
	s.EqualError(err, "failed to retrieve ingress: ingresses.networking.k8s.io \"my-ingress\" not found")
	s.True(errors.IsNotFound(err))
	s.Zero(result)
}

func (s *IngressReconcilerTestSuite) TestWithoutAnnotation() {
	ingress := ingress.DeepCopyObject().(*networkingv1.Ingress)

	err := s.Create(context.Background(), ingress)
	s.Require().NoError(err)

	result, err := s.controller.ReconcileIngress(context.Background(), requestIngress)
	s.NoError(err)
	s.Zero(result)

	err = s.Get(context.Background(), requestIngress.NamespacedName, ingress)
	s.Require().NoError(err)
	s.Empty(ingress.GetFinalizers(), "should not have finalizer")
}

func (s *IngressReconcilerTestSuite) TestPendingDeletion() {
	s.TestAddAnnotation()

	ingress := ingress.DeepCopyObject().(*networkingv1.Ingress)
	err := s.Delete(context.Background(), ingress)
	s.Require().NoError(err)

	result, err := s.controller.ReconcileIngress(context.Background(), requestIngress)
	s.NoError(err)
	s.Zero(result)

	// NOTE: as the finalizer should have been removed, the resource is
	// expected to be deleted from the cluster.
	err = s.Get(context.Background(), requestIngress.NamespacedName, ingress)
	s.Require().Error(err)
	s.True(errors.IsNotFound(err))
}

func (s *IngressReconcilerTestSuite) TestAddAnnotation() {
	ingress := ingress.DeepCopyObject().(*networkingv1.Ingress)
	ingress.Annotations = map[string]string{
		"lemonldap-ng.org/locationRules":   "default: newvalue",
		"lemonldap-ng.org/exportedHeaders": "Auth-Mail: newvalue\nAuth-User: newvalue\n",
		"lemonldap-ng.org/vhostOptions":    "vhostType: newvalue",
	}

	err := s.Create(context.Background(), ingress)
	s.Require().NoError(err)

	result, err := s.controller.ReconcileIngress(context.Background(), requestIngress)
	s.NoError(err)
	s.Zero(result)

	s.Equal(lemonldap.VHosts{
		"domain.example.com": &lemonldap.VHost{
			ExportedHeaders: map[string]any{"Auth-Mail": "newvalue", "Auth-User": "newvalue"},
			LocationRules:   map[string]any{"default": "newvalue"},
			VhostOptions:    map[string]any{"vhostType": "newvalue"},
		},
	}, s.controller.vhosts)

	err = s.Get(context.Background(), requestIngress.NamespacedName, ingress)
	s.Require().NoError(err)
	s.Contains(ingress.GetFinalizers(), lemonFinalizer)
}

func (s *IngressReconcilerTestSuite) TestUpdateAnnotation() {
	s.TestAddAnnotation()

	ingress := ingress.DeepCopyObject().(*networkingv1.Ingress)
	err := s.Get(context.Background(), requestIngress.NamespacedName, ingress)
	s.Require().NoError(err)

	ingress.Annotations = map[string]string{
		"lemonldap-ng.org/locationRules":   "default: updatedvalue",
		"lemonldap-ng.org/exportedHeaders": "Auth-Mail: updatedvalue\nAuth-User: updatedvalue\n",
		"lemonldap-ng.org/vhostOptions":    "vhostType: updatedvalue",
	}

	err = s.Update(context.Background(), ingress)
	s.Require().NoError(err)

	result, err := s.controller.ReconcileIngress(context.Background(), requestIngress)
	s.NoError(err)
	s.Zero(result)

	s.Equal(lemonldap.VHosts{
		"domain.example.com": &lemonldap.VHost{
			ExportedHeaders: map[string]any{"Auth-Mail": "updatedvalue", "Auth-User": "updatedvalue"},
			LocationRules:   map[string]any{"default": "updatedvalue"},
			VhostOptions:    map[string]any{"vhostType": "updatedvalue"},
		},
	}, s.controller.vhosts)

	err = s.Get(context.Background(), requestIngress.NamespacedName, ingress)
	s.Require().NoError(err)
	s.Contains(ingress.GetFinalizers(), lemonFinalizer)
}

func (s *IngressReconcilerTestSuite) TestRemoveAnnotation() {
	s.TestAddAnnotation()

	ingress := ingress.DeepCopyObject().(*networkingv1.Ingress)
	err := s.Get(context.Background(), requestIngress.NamespacedName, ingress)
	s.Require().NoError(err)

	delete(ingress.Annotations, locationRulesAnnotationKey)

	err = s.Update(context.Background(), ingress)
	s.Require().NoError(err)

	result, err := s.controller.ReconcileIngress(context.Background(), requestIngress)
	s.NoError(err)
	s.Zero(result)

	s.Equal(lemonldap.VHosts{
		"domain.example.com": &lemonldap.VHost{
			ExportedHeaders: map[string]any{"Auth-Mail": "newvalue", "Auth-User": "newvalue"},
			VhostOptions:    map[string]any{"vhostType": "newvalue"},
		},
	}, s.controller.vhosts)

	err = s.Get(context.Background(), requestIngress.NamespacedName, ingress)
	s.Require().NoError(err)
	s.Contains(ingress.GetFinalizers(), lemonFinalizer)
}

func (s *IngressReconcilerTestSuite) TestRemoveAllAnnotations() {
	s.TestAddAnnotation()

	ingress := ingress.DeepCopyObject().(*networkingv1.Ingress)
	err := s.Get(context.Background(), requestIngress.NamespacedName, ingress)
	s.Require().NoError(err)

	ingress.Annotations = map[string]string{}

	err = s.Update(context.Background(), ingress)
	s.Require().NoError(err)

	result, err := s.controller.ReconcileIngress(context.Background(), requestIngress)
	s.NoError(err)
	s.Zero(result)

	s.NotContains(s.controller.vhosts, "domain.example.com")

	err = s.Get(context.Background(), requestIngress.NamespacedName, ingress)
	s.Require().NoError(err)
	s.Empty(ingress.GetFinalizers())
}

func TestIngressReconcilerTestSuite(t *testing.T) {
	t.Parallel()
	suite.Run(t, new(IngressReconcilerTestSuite))
}
