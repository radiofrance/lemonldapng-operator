package controller

import (
	"fmt"

	"go.uber.org/multierr"
	"k8s.io/apimachinery/pkg/types"
)

type (
	// Config contains all properties required by the controller to reconcile
	// the Lemon configuration.
	Config struct {
		LemonHandler LemonHandler
	}

	// LemonHandler contains information about the LemonLDAP handler.
	LemonHandler struct {
		Configmap  LemonHandlerConfigmap
		Deployment LemonHandlerDeployment
	}

	// LemonHandlerConfigmap contains all properties required to store the
	// final LemonLDAP configuration.
	LemonHandlerConfigmap struct {
		Name      string
		Namespace string
		Key       string
	}

	// LemonHandlerDeployment contains all properties required to reload the
	// lemon handler deployment.
	LemonHandlerDeployment struct {
		Name        string
		Namespace   string
		ForceReload bool
	}
)

func (config Config) validate() error {
	var errs error

	if err := config.LemonHandler.validate(); err != nil {
		errs = multierr.Append(errs, err)
	}
	return errs
}

func (handler LemonHandler) validate() error {
	var errs error

	if err := handler.Configmap.validate(); err != nil {
		errs = multierr.Append(errs, err)
	}
	if err := handler.Deployment.validate(); err != nil {
		errs = multierr.Append(errs, err)
	}
	return errs
}

func (config LemonHandlerConfigmap) validate() error {
	var err error

	if config.Name == "" {
		err = multierr.Append(err, fmt.Errorf("LemonLDAP configmap name is required"))
	}
	if config.Namespace == "" {
		err = multierr.Append(err, fmt.Errorf("LemonLDAP configmap namespace is required"))
	}
	return err
}

// NamespacedName returns a valid NamespacedName based on the given configuration.
func (config LemonHandlerConfigmap) NamespacedName() types.NamespacedName {
	return types.NamespacedName{
		Namespace: config.Namespace,
		Name:      config.Name,
	}
}

func (deploy LemonHandlerDeployment) validate() error {
	if !deploy.ForceReload {
		return nil
	}

	var err error

	if deploy.Name == "" {
		err = multierr.Append(err, fmt.Errorf("LemonLDAP deployment name is required when force reload is enabled"))
	}
	if deploy.Name == "" {
		err = multierr.Append(err, fmt.Errorf("LemonLDAP deployment namespace is required when force reload is enabled"))
	}
	return err
}
