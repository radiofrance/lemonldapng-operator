//nolint:testpackage
package controller

import (
	"context"
	"testing"

	"github.com/stretchr/testify/suite"
	lemonldap "gitlab.com/radiofrance/lemonldapng-operator/internal/lemonldapng/config"
	appsv1 "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/types"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
)

type ConfigmapReconcilerTestSuite struct {
	ControllerTestSuiteBase
}

func (s *ConfigmapReconcilerTestSuite) SetupTest() {
	s.ControllerTestSuiteBase.SetupTest()

	s.controller.vhosts = lemonldap.VHosts{
		"domain.example.com": &lemonldap.VHost{
			ExportedHeaders: map[string]any{"Auth-Mail": "newvalue", "Auth-User": "newvalue"},
			LocationRules:   map[string]any{"default": "newvalue"},
			VhostOptions:    map[string]any{"vhostType": "newvalue"},
		},
	}
}

func (s *ConfigmapReconcilerTestSuite) TestIgnoreReconciliationNotTargetConfigmap() {
	configmap := &corev1.ConfigMap{}

	err := s.Get(context.Background(), requestConfigmap.NamespacedName, configmap)
	s.Require().Error(err)
	s.Require().True(errors.IsNotFound(err))

	request := reconcile.Request{NamespacedName: types.NamespacedName{
		Namespace: "default",
		Name:      "not-target-configmap",
	}}
	result, err := s.controller.ReconcileConfigmap(context.Background(), request)
	s.NoError(err)
	s.Zero(result)

	err = s.Get(context.Background(), requestConfigmap.NamespacedName, configmap)
	s.Require().Error(err)
	s.Require().True(errors.IsNotFound(err))
}

func (s *ConfigmapReconcilerTestSuite) TestCreateConfigmapWhenNotFound() {
	configmap := &corev1.ConfigMap{}

	err := s.Get(context.Background(), requestConfigmap.NamespacedName, configmap)
	s.Require().Error(err)
	s.Require().True(errors.IsNotFound(err))

	result, err := s.controller.ReconcileConfigmap(context.Background(), requestConfigmap)
	s.NoError(err)
	s.Zero(result)

	err = s.Get(context.Background(), requestConfigmap.NamespacedName, configmap)
	s.Require().NoError(err)
	s.Contains(configmap.Data, "lmConf-1.json")
	s.JSONEq(`{
		"baseConfig": "value",
		"locationRules": {
			"domain.example.com": {
				"default": "newvalue"
			}
		},
		"exportedHeaders": {
			"domain.example.com": {
				"Auth-Mail": "newvalue",
				"Auth-User": "newvalue"
			}
		},
		"vhostOptions": {
			"domain.example.com": {
				"vhostType": "newvalue"
			}
		}
	}`, configmap.Data["lmConf-1.json"])
}

func (s *ConfigmapReconcilerTestSuite) TestUpdateVhost() {
	s.TestCreateConfigmapWhenNotFound()

	s.controller.vhosts.OverrideHost("domain.example.com", lemonldap.VHost{
		ExportedHeaders: map[string]any{"Auth-Mail": "updatedvalue", "Auth-User": "updatedvalue"},
		LocationRules:   map[string]any{"default": "updatedvalue"},
		VhostOptions:    map[string]any{"vhostType": "updatedvalue"},
	})

	result, err := s.controller.ReconcileConfigmap(context.Background(), requestConfigmap)
	s.NoError(err)
	s.Zero(result)

	configmap := &corev1.ConfigMap{}
	err = s.Get(context.Background(), requestConfigmap.NamespacedName, configmap)
	s.Require().NoError(err)
	s.Contains(configmap.Data, "lmConf-1.json")
	s.JSONEq(`{
		"baseConfig": "value",
		"locationRules": {
			"domain.example.com": {
				"default": "updatedvalue"
			}
		},
		"exportedHeaders": {
			"domain.example.com": {
				"Auth-Mail": "updatedvalue",
				"Auth-User": "updatedvalue"
			}
		},
		"vhostOptions": {
			"domain.example.com": {
				"vhostType": "updatedvalue"
			}
		}
	}`, configmap.Data["lmConf-1.json"])
}

func (s *ConfigmapReconcilerTestSuite) TestRemoveVhostProperty() {
	s.TestCreateConfigmapWhenNotFound()

	s.controller.vhosts.OverrideHost("domain.example.com", lemonldap.VHost{
		LocationRules: map[string]any{},
	})

	result, err := s.controller.ReconcileConfigmap(context.Background(), requestConfigmap)
	s.NoError(err)
	s.Zero(result)

	configmap := &corev1.ConfigMap{}
	err = s.Get(context.Background(), requestConfigmap.NamespacedName, configmap)
	s.Require().NoError(err)
	s.Contains(configmap.Data, "lmConf-1.json")
	s.JSONEq(`{
		"baseConfig": "value",
		"locationRules": {
			"domain.example.com": {}
		},
		"exportedHeaders": {
			"domain.example.com": {
				"Auth-Mail": "newvalue",
				"Auth-User": "newvalue"
			}
		},
		"vhostOptions": {
			"domain.example.com": {
				"vhostType": "newvalue"
			}
		}
	}`, configmap.Data["lmConf-1.json"])
}

func (s *ConfigmapReconcilerTestSuite) TestTargetConfigManuallyUpdated() {
	s.TestCreateConfigmapWhenNotFound()

	configmap := &corev1.ConfigMap{}
	err := s.Get(context.Background(), requestConfigmap.NamespacedName, configmap)
	s.Require().NoError(err)

	configmap.Data["lmConf-1.json"] = `{
		"baseConfig": "what-could-go-wrong",
		"locationRules": {
			"domain.example.com": {
				"default": "newvalue"
			}
		},
		"exportedHeaders": {
			"domain.example.com": {
				"Auth-Mail": "newvalue",
				"Auth-User": "newvalue"
			}
		},
		"vhostOptions": {
			"domain.example.com": {
				"vhostType": "newvalue"
			}
		}
	}`

	err = s.Update(context.Background(), configmap)
	s.Require().NoError(err)

	_, err = s.controller.ReconcileConfigmap(context.Background(), requestConfigmap)
	s.Require().NoError(err)

	err = s.Get(context.Background(), requestConfigmap.NamespacedName, configmap)
	s.Require().NoError(err)
	s.Contains(configmap.Data, "lmConf-1.json")
	s.JSONEq(`{
		"baseConfig": "value",
		"locationRules": {
			"domain.example.com": {
				"default": "newvalue"
			}
		},
		"exportedHeaders": {
			"domain.example.com": {
				"Auth-Mail": "newvalue",
				"Auth-User": "newvalue"
			}
		},
		"vhostOptions": {
			"domain.example.com": {
				"vhostType": "newvalue"
			}
		}
	}`, configmap.Data["lmConf-1.json"])
}

func (s *ConfigmapReconcilerTestSuite) TestHandleBaseConfigChanges() {
	s.TestCreateConfigmapWhenNotFound()

	err := s.updateBaseConfiguration(`{
		"baseConfig": "updatedvalue",
		"locationRules":{},
		"exportedHeaders":{},
		"vhostOptions":{}
	}`)
	s.Require().NoError(err)
	<-s.controller.watcher.Changed()

	result, err := s.controller.ReconcileConfigmap(context.Background(), requestConfigmap)
	s.NoError(err)
	s.Zero(result)

	configmap := &corev1.ConfigMap{}
	err = s.Get(context.Background(), requestConfigmap.NamespacedName, configmap)
	s.Require().NoError(err)
	s.Contains(configmap.Data, "lmConf-1.json")
	s.JSONEq(`{
		"baseConfig": "updatedvalue",
		"locationRules": {
			"domain.example.com": {
				"default": "newvalue"
			}
		},
		"exportedHeaders": {
			"domain.example.com": {
				"Auth-Mail": "newvalue",
				"Auth-User": "newvalue"
			}
		},
		"vhostOptions": {
			"domain.example.com": {
				"vhostType": "newvalue"
			}
		}
	}`, configmap.Data["lmConf-1.json"])
}

func (s *ConfigmapReconcilerTestSuite) TestHandleDeploymentRollout() {
	deployment := &appsv1.Deployment{
		TypeMeta: metav1.TypeMeta{
			APIVersion: "apps/v1",
			Kind:       "Deployment",
		},
		ObjectMeta: metav1.ObjectMeta{
			Name:      "lemonldapng-handler",
			Namespace: "default",
		},
		Spec: appsv1.DeploymentSpec{
			Template: corev1.PodTemplateSpec{
				ObjectMeta: metav1.ObjectMeta{
					Annotations: map[string]string{
						"baseconfhash": "initial-hash",
					},
				},
			},
		},
	}
	s.controller.config.LemonHandler.Deployment.ForceReload = true
	s.controller.config.LemonHandler.Deployment.Name = deployment.ObjectMeta.Name
	s.controller.config.LemonHandler.Deployment.Namespace = deployment.ObjectMeta.Namespace

	err := s.Create(context.Background(), deployment)
	s.Require().NoError(err)

	result, err := s.controller.ReconcileConfigmap(context.Background(), requestConfigmap)
	s.NoError(err)
	s.Zero(result)

	err = s.Get(context.Background(), types.NamespacedName{
		Name:      deployment.Name,
		Namespace: deployment.Namespace,
	}, deployment)
	s.Require().NoError(err)
	s.Contains(deployment.Spec.Template.Annotations, "baseconfhash")
	s.Equal("nSMmoqp5PmVbR4t6KqHqo2wJjJE=", deployment.Spec.Template.Annotations["baseconfhash"])
}

func TestConfigmapReconcilerTestSuite(t *testing.T) {
	t.Parallel()
	suite.Run(t, new(ConfigmapReconcilerTestSuite))
}
