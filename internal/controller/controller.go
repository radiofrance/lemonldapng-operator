package controller

import (
	"context"
	"fmt"

	lemonldap "gitlab.com/radiofrance/lemonldapng-operator/internal/lemonldapng/config"
	"gitlab.com/radiofrance/lemonldapng-operator/internal/watcher"
	corev1 "k8s.io/api/core/v1"
	networkingv1 "k8s.io/api/networking/v1"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	"sigs.k8s.io/controller-runtime/pkg/manager"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
	"sigs.k8s.io/controller-runtime/pkg/source"
)

type Controller struct {
	client  client.Client
	config  Config
	watcher *watcher.File

	vhosts lemonldap.VHosts
}

const (
	lemonFinalizer = "lemonldap-ng.org/finalizer"
)

func (ctrl *Controller) Reconcile(ctx context.Context, request reconcile.Request) (reconcile.Result, error) {
	return ctrl.ReconcileIngress(ctx, request)
}

// NewController creates a new Controller object.
func NewController(manager manager.Manager, config Config, watcher *watcher.File) (*Controller, error) {
	if err := config.validate(); err != nil {
		return nil, fmt.Errorf("invalid configuration: %w", err)
	}

	ctrl := &Controller{
		client:  manager.GetClient(),
		config:  config,
		watcher: watcher,

		vhosts: lemonldap.VHosts{},
	}

	{ // Configure ingress reconciler
		control, err := controller.New(
			"lemonldap-ng-controller/ingress",
			manager,
			controller.Options{Reconciler: reconcile.Func(ctrl.ReconcileIngress)},
		)
		if err != nil {
			return nil, fmt.Errorf("ingress reconcilier creation failed: %w", err)
		}

		if err := control.Watch(&source.Kind{Type: &networkingv1.Ingress{}}, &handler.EnqueueRequestForObject{}); err != nil {
			return nil, fmt.Errorf("ingress reconcilier configuration failed: %w", err)
		}
	}

	{ // Configure configmap reconciler
		control, err := controller.New(
			"lemonldap-ng-controller/configmap",
			manager,
			controller.Options{Reconciler: reconcile.Func(ctrl.ReconcileConfigmap)},
		)
		if err != nil {
			return nil, fmt.Errorf("configmap reconcilier creation failed: %w", err)
		}

		if err := control.Watch(&source.Kind{Type: &corev1.ConfigMap{}}, &handler.EnqueueRequestForObject{}); err != nil {
			return nil, fmt.Errorf("configmap reconcilier configuration failed: %w", err)
		}
	}

	return ctrl, nil
}
