package controller

import (
	lemonldap "gitlab.com/radiofrance/lemonldapng-operator/internal/lemonldapng/config"
	"go.uber.org/multierr"
	"gopkg.in/yaml.v3"
)

const (
	exportHeadersAnnotationKey = "lemonldap-ng.org/exportedHeaders"
	locationRulesAnnotationKey = "lemonldap-ng.org/locationRules"
	vhostOptionsAnnotationKey  = "lemonldap-ng.org/vhostOptions"
)

// hasLemonAnnotations returns true if the given map contains at least one
// LemonLDAP::NG annotation. Otherwise, it returns false.
func hasLemonAnnotations(annotations map[string]string) bool {
	switch {
	case annotations[exportHeadersAnnotationKey] != "":
		return true
	case annotations[locationRulesAnnotationKey] != "":
		return true
	case annotations[vhostOptionsAnnotationKey] != "":
		return true
	default:
		return false
	}
}

// extractVHostFromAnnotations extracts virtual host configuration from ingress annotations.
func extractVHostFromAnnotations(annotations map[string]string) (lemonldap.VHost, error) {
	var errs error

	config := lemonldap.VHost{}
	if annotation, exist := annotations[exportHeadersAnnotationKey]; exist {
		if err := yaml.Unmarshal([]byte(annotation), &config.ExportedHeaders); err != nil {
			errs = multierr.Append(errs, err)
		}
	}

	if annotation, exist := annotations[locationRulesAnnotationKey]; exist {
		if err := yaml.Unmarshal([]byte(annotation), &config.LocationRules); err != nil {
			errs = multierr.Append(errs, err)
		}
	}

	if annotation, exist := annotations[vhostOptionsAnnotationKey]; exist {
		if err := yaml.Unmarshal([]byte(annotation), &config.VhostOptions); err != nil {
			errs = multierr.Append(errs, err)
		}
	}

	return config, errs
}
