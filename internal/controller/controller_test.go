//nolint:testpackage
package controller

import (
	"io"
	"os"

	"github.com/stretchr/testify/suite"
	lemonldap "gitlab.com/radiofrance/lemonldapng-operator/internal/lemonldapng/config"
	"gitlab.com/radiofrance/lemonldapng-operator/internal/watcher"
	networkingv1 "k8s.io/api/networking/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/types"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/client/fake"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
)

const (
	jsonRawConfig = `{
		"baseConfig": "value",
		"locationRules":{},
		"exportedHeaders":{},
		"vhostOptions":{}
	}`
)

var (
	ingress = &networkingv1.Ingress{
		TypeMeta: metav1.TypeMeta{
			Kind:       "Ingress",
			APIVersion: "networking.k8s.io/v1",
		},
		ObjectMeta: metav1.ObjectMeta{
			Name:      "my-ingress",
			Namespace: "default",
		},
		Spec: networkingv1.IngressSpec{
			Rules: []networkingv1.IngressRule{
				{Host: "domain.example.com"},
				{Host: "otherdomain.example.com"},
			},
		},
	}
	requestIngress = reconcile.Request{
		NamespacedName: types.NamespacedName{
			Namespace: ingress.Namespace,
			Name:      ingress.Name,
		},
	}
	requestConfigmap = reconcile.Request{
		NamespacedName: types.NamespacedName{
			Namespace: "default",
			Name:      "my-configmap",
		},
	}
)

// ControllerTestSuite is only used to share setup and teardown mechanism
// between test suites.
type ControllerTestSuiteBase struct {
	suite.Suite
	client.Client

	controller              *Controller
	updateBaseConfiguration func(data string) error
}

func (s *ControllerTestSuiteBase) SetupTest() {
	s.Client = fake.NewClientBuilder().Build()

	// Generate base configuration file
	file, err := os.CreateTemp(s.T().TempDir(), "")
	s.Require().NoError(err)

	_, err = io.WriteString(file, jsonRawConfig)
	s.Require().NoError(err)
	s.Require().NoError(file.Close())

	s.updateBaseConfiguration = func(data string) error {
		return os.WriteFile(file.Name(), []byte(data), 0o644) //nolint:gosec
	}

	watcher, err := watcher.NewFile(file.Name())
	s.Require().NoError(err)

	s.controller = &Controller{
		client: s.Client,
		config: Config{
			LemonHandler: LemonHandler{
				Configmap: LemonHandlerConfigmap{
					Namespace: requestConfigmap.Namespace,
					Name:      requestConfigmap.Name,
					Key:       "lmConf-1.json",
				},
			},
		},
		watcher: watcher,
		vhosts:  lemonldap.VHosts{},
	}
}
