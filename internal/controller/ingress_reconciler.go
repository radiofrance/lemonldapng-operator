package controller

import (
	"context"
	"fmt"

	"github.com/sirupsen/logrus"
	networkingv1 "k8s.io/api/networking/v1"
	"sigs.k8s.io/controller-runtime/pkg/controller/controllerutil"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
)

// ReconcileIngress reconciles all ingress to update the LemonLDAP handler configuration.
//
//nolint:funlen,cyclop
func (ctrl *Controller) ReconcileIngress(ctx context.Context, request reconcile.Request) (reconcile.Result, error) {
	ingress := &networkingv1.Ingress{}
	if err := ctrl.client.Get(ctx, request.NamespacedName, ingress); err != nil {
		logrus.Errorf("failed to retrieve ingress: %v", err)
		return reconcile.Result{}, fmt.Errorf("failed to retrieve ingress: %w", err)
	}

	// If the ingress doesn't have lemonldap-ng.org annotations means that this
	// ingress is not managed nor these annotations has been removed recently.
	// We handle the second case like we handle an ingress removal.
	if !hasLemonAnnotations(ingress.Annotations) || ingress.DeletionTimestamp != nil {
		if !controllerutil.ContainsFinalizer(ingress, lemonFinalizer) {
			return reconcile.Result{}, nil
		}

		// In the case where annotations are deleted, we must also :
		// - delete the host reference inside our local configuration cache
		// - delete our finalizer to avoid blocking the ingress' destruction process
		logrus.Infof("Starting reconciliation for ingress %s", request)

		host := ingress.Spec.Rules[0].Host
		delete(ctrl.vhosts, host)

		_, err := ctrl.ReconcileConfigmap(
			ctx,
			reconcile.Request{NamespacedName: ctrl.config.LemonHandler.Configmap.NamespacedName()},
		)
		if err != nil {
			logrus.Errorf("lemonldap handler configuration update failed: %s", err)
			return reconcile.Result{}, fmt.Errorf("lemonldap handler configuration update failed: %w", err)
		}

		controllerutil.RemoveFinalizer(ingress, lemonFinalizer)
		err = ctrl.client.Update(ctx, ingress)
		if err != nil {
			logrus.Errorf("ingress %s/%s update failed: %s", ingress.Namespace, ingress.Name, err)
			return reconcile.Result{}, fmt.Errorf("ingress %s/%s update failed: %w", ingress.Namespace, ingress.Name, err)
		}

		return reconcile.Result{}, nil
	}

	logrus.Infof("Starting reconciliation for ingress %s", request)

	config, err := extractVHostFromAnnotations(ingress.Annotations)
	if err != nil {
		logrus.Errorf("annotation extraction for ingress %s/%s failed: %s", ingress.Namespace, ingress.Name, err)
		return reconcile.Result{}, fmt.Errorf(
			"annotation extraction for ingress %s/%s failed: %w",
			ingress.Namespace, ingress.Name,
			err,
		)
	}

	host := ingress.Spec.Rules[0].Host
	ctrl.vhosts[host] = &config

	_, err = ctrl.ReconcileConfigmap(
		ctx,
		reconcile.Request{NamespacedName: ctrl.config.LemonHandler.Configmap.NamespacedName()},
	)
	if err != nil {
		logrus.Errorf("lemonldap handler configuration update failed: %s", err)
		return reconcile.Result{}, fmt.Errorf("lemonldap handler configuration update failed: %w", err)
	}

	if !controllerutil.ContainsFinalizer(ingress, lemonFinalizer) {
		controllerutil.AddFinalizer(ingress, lemonFinalizer)
		err = ctrl.client.Update(ctx, ingress)
		if err != nil {
			logrus.Errorf("ingress %s/%s update failed: %s", ingress.Namespace, ingress.Name, err)
			return reconcile.Result{}, fmt.Errorf("ingress %s/%s update failed: %w", ingress.Namespace, ingress.Name, err)
		}
	}

	logrus.Infof("Ingress %s reconcilied", request)

	return reconcile.Result{}, nil
}
