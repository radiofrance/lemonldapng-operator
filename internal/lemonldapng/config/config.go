package config

import (
	"encoding/json"
)

const (
	exportedHeadersKey = "exportedHeaders"
	locationRulesKey   = "locationRules"
	vhostOptionsKey    = "vhostOptions"
)

type (
	// Config groups vhost LemonLDAP::NG configuration with unmanaged raw
	// configuration.
	//
	// NOTE: this structure was preferred to using the LemonLDAP::NG
	// configuration because it is easier to create, read, modify and delete
	// configurations using vhost as a key than to follow the official
	// LemonLDAP::NG configuration structure.
	Config struct {
		VHosts
		raw map[string]any
	}

	// VHosts groups all virtual host configuration by host name.
	VHosts map[string]*VHost

	// VHost represents a LemonLDAP::NG configuration for a specific
	// virtual host.
	VHost struct {
		ExportedHeaders map[string]any
		LocationRules   map[string]any
		VhostOptions    map[string]any
	}
)

// NewConfigFromJSON parses the JSON-encoded data and stores the result into a
// Config object.
func NewConfigFromJSON(data []byte) (*Config, error) {
	config := &Config{VHosts: VHosts{}, raw: map[string]any{}}

	if data == nil {
		return config, nil
	}

	config.raw = map[string]any{}
	if err := json.Unmarshal(data, &config.raw); err != nil {
		return nil, err
	}

	if exportedHeaders, exists := config.raw[exportedHeadersKey]; exists {
		exportedHeaders, _ := exportedHeaders.(map[string]any)
		for host, property := range exportedHeaders {
			headers, _ := property.(map[string]any)
			config.OverrideHost(host, VHost{ExportedHeaders: headers})
		}
		delete(config.raw, exportedHeadersKey)
	}

	if locationRules, exists := config.raw[locationRulesKey]; exists {
		locationRules, _ := locationRules.(map[string]any)
		for host, property := range locationRules {
			rules, _ := property.(map[string]any)
			config.OverrideHost(host, VHost{LocationRules: rules})
		}
		delete(config.raw, locationRulesKey)
	}

	if vhostOptions, exists := config.raw[vhostOptionsKey]; exists {
		vhostOptions, _ := vhostOptions.(map[string]any)
		for host, property := range vhostOptions {
			options, _ := property.(map[string]any)
			config.OverrideHost(host, VHost{VhostOptions: options})
		}
		delete(config.raw, vhostOptionsKey)
	}

	return config, nil
}

// MarshalJSON implements the Marshaler interface to convert Config into a
// valid LemonLDAP::ND configuration.
func (conf Config) MarshalJSON() ([]byte, error) {
	exportedHeaders := map[string]any{}
	locationRules := map[string]any{}
	vhostOptions := map[string]any{}
	for host, config := range conf.VHosts {
		exportedHeaders[host] = config.ExportedHeaders
		locationRules[host] = config.LocationRules
		vhostOptions[host] = config.VhostOptions
	}

	raw := map[string]any{
		exportedHeadersKey: exportedHeaders,
		locationRulesKey:   locationRules,
		vhostOptionsKey:    vhostOptions,
	}
	for k, v := range conf.raw {
		raw[k] = v
	}

	return json.Marshal(raw)
}

// OverrideHost overrides one or several properties for a given hostname.
func (v VHosts) OverrideHost(hostname string, vhosts VHost) {
	if _, exists := v[hostname]; !exists {
		v[hostname] = &VHost{}
	}

	if vhosts.ExportedHeaders != nil {
		v[hostname].ExportedHeaders = vhosts.ExportedHeaders
	}
	if vhosts.LocationRules != nil {
		v[hostname].LocationRules = vhosts.LocationRules
	}
	if vhosts.VhostOptions != nil {
		v[hostname].VhostOptions = vhosts.VhostOptions
	}
}

// Overwrite returns a copy of the current configuration of the virtual hosts
// replaced by the given ones.
func (v VHosts) Overwrite(overwrite VHosts) VHosts {
	overwritten := VHosts{}

	for _, vhosts := range []VHosts{v, overwrite} {
		for hostname, vhost := range vhosts {
			overwritten[hostname] = vhost
		}
	}

	return overwritten
}
