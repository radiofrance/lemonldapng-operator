//nolint:funlen,lll,testpackage
package config

import (
	"encoding/json"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestNewConfigFromJSON(t *testing.T) {
	t.Parallel()

	tcases := []struct {
		name string
		json []byte

		expected   *Config
		shouldFail bool
		error      string
	}{
		{
			name: "Simple JSON configuration",
			json: []byte(`{"someProperty":"some value","exportedHeaders":{"domain1.example.com":{"Auth-Cn":"$cn","Auth-Groups":"$groups","Auth-Mail":"$mail","Auth-User":"$uid"},"domain2.example.com":{"Auth-Cn":"$cn","Auth-Groups":"$groups","Auth-Mail":"$mail","Auth-User":"$uid"}},"locationRules":{"domain1.example.com":{"default":"inGroup('role-admin')"},"domain2.example.com":{"default":"inGroup('role-user')"},"domain3.example.com":{}},"vhostOptions":{"domain1.example.com":{"vhostHttps":1,"vhostMaintenance":0,"vhostPort":443,"vhostType":"CDA"},"domain2.example.com":{"vhostHttps":1,"vhostMaintenance":0,"vhostPort":443,"vhostType":"CDA"}}}`),
			expected: &Config{
				VHosts: VHosts{
					"domain1.example.com": {
						ExportedHeaders: map[string]any{
							"Auth-Cn":     "$cn",
							"Auth-Groups": "$groups",
							"Auth-Mail":   "$mail",
							"Auth-User":   "$uid",
						},
						LocationRules: map[string]any{
							"default": "inGroup('role-admin')",
						},
						VhostOptions: map[string]any{
							"vhostHttps":       float64(1),
							"vhostMaintenance": float64(0),
							"vhostPort":        float64(443),
							"vhostType":        "CDA",
						},
					},
					"domain2.example.com": {
						ExportedHeaders: map[string]any{
							"Auth-Cn":     "$cn",
							"Auth-Groups": "$groups",
							"Auth-Mail":   "$mail",
							"Auth-User":   "$uid",
						},
						LocationRules: map[string]any{
							"default": "inGroup('role-user')",
						},
						VhostOptions: map[string]any{
							"vhostHttps":       float64(1),
							"vhostMaintenance": float64(0),
							"vhostPort":        float64(443),
							"vhostType":        "CDA",
						},
					},
					"domain3.example.com": {
						LocationRules: map[string]any{},
					},
				},
				raw: map[string]any{
					"someProperty": "some value",
				},
			},
		},
		{
			name:     "Nil JSON",
			json:     nil,
			expected: &Config{VHosts: VHosts{}, raw: map[string]any{}},
		},
		{
			name:       "Invalid JSON",
			json:       []byte("invalid JSON"),
			shouldFail: true,
			error:      "invalid character 'i' looking for beginning of value",
		},
	}

	for _, tcase := range tcases {
		tcase := tcase
		t.Run(tcase.name, func(t *testing.T) {
			t.Parallel()
			config, err := NewConfigFromJSON(tcase.json)

			if tcase.shouldFail {
				assert.EqualError(t, err, tcase.error)
				assert.Nil(t, config)
			} else {
				assert.NoError(t, err)
				assert.Equal(t, config, tcase.expected)
			}
		})
	}
}

func TestConfig_MarshalJSON(t *testing.T) {
	t.Parallel()

	tcases := []struct {
		name     string
		config   Config
		expected string
	}{
		{
			name: "Simple config",
			config: Config{
				VHosts: VHosts{
					"domain1.example.com": {
						ExportedHeaders: map[string]any{
							"Auth-Cn":     "$cn",
							"Auth-Groups": "$groups",
							"Auth-Mail":   "$mail",
							"Auth-User":   "$uid",
						},
						LocationRules: map[string]any{
							"default": "inGroup('role-admin')",
						},
						VhostOptions: map[string]any{
							"vhostHttps":       float64(1),
							"vhostMaintenance": float64(0),
							"vhostPort":        float64(443),
							"vhostType":        "CDA",
						},
					},
					"domain2.example.com": {
						ExportedHeaders: map[string]any{
							"Auth-Cn":     "$cn",
							"Auth-Groups": "$groups",
							"Auth-Mail":   "$mail",
							"Auth-User":   "$uid",
						},
						LocationRules: map[string]any{
							"default": "inGroup('role-user')",
						},
						VhostOptions: map[string]any{
							"vhostHttps":       float64(1),
							"vhostMaintenance": float64(0),
							"vhostPort":        float64(443),
							"vhostType":        "CDA",
						},
					},
					"domain3.example.com": {
						LocationRules: map[string]any{},
					},
				},
				raw: map[string]any{
					"someProperty": "some value",
				},
			},
			expected: `{
  "someProperty": "some value",
  "exportedHeaders": {
    "domain1.example.com": {
      "Auth-Cn": "$cn",
      "Auth-Groups": "$groups",
      "Auth-Mail": "$mail",
      "Auth-User": "$uid"
    },
    "domain2.example.com": {
      "Auth-Cn": "$cn",
      "Auth-Groups": "$groups",
      "Auth-Mail": "$mail",
      "Auth-User": "$uid"
    },
    "domain3.example.com": null
  },
  "locationRules": {
    "domain1.example.com": {
      "default": "inGroup('role-admin')"
    },
    "domain2.example.com": {
      "default": "inGroup('role-user')"
    },
    "domain3.example.com": {}
  },
  "vhostOptions": {
    "domain1.example.com": {
      "vhostHttps": 1,
      "vhostMaintenance": 0,
      "vhostPort": 443,
      "vhostType": "CDA"
    },
    "domain2.example.com": {
      "vhostHttps": 1,
      "vhostMaintenance": 0,
      "vhostPort": 443,
      "vhostType": "CDA"
    },
    "domain3.example.com": null
  }
}`,
		},
		{
			name:     "Empty configuration",
			config:   Config{},
			expected: `{"exportedHeaders":{},"locationRules":{},"vhostOptions":{}}`,
		},
	}

	for _, tcase := range tcases {
		tcase := tcase
		t.Run(tcase.name, func(t *testing.T) {
			t.Parallel()
			raw, err := json.Marshal(tcase.config)
			require.NoError(t, err)
			assert.JSONEq(t, tcase.expected, string(raw))
		})
	}
}

func TestVHosts_OverrideHost(t *testing.T) {
	t.Parallel()

	raw := `{"exportedHeaders":{"domain1.example.com":{"header":"some header"}},"locationRules":{"domain1.example.com":{"location":"some location"}},"vhostOptions":{"domain1.example.com":{"vhost":"some option"}}}`
	conf, err := NewConfigFromJSON([]byte(raw))
	require.NoError(t, err)

	assert.Contains(t, conf.VHosts, "domain1.example.com")
	assert.Equal(t, "some header", conf.VHosts["domain1.example.com"].ExportedHeaders["header"])
	assert.Equal(t, "some location", conf.VHosts["domain1.example.com"].LocationRules["location"])
	assert.Equal(t, "some option", conf.VHosts["domain1.example.com"].VhostOptions["vhost"])

	conf.OverrideHost("domain1.example.com", VHost{VhostOptions: map[string]any{"vhost": "other option"}})
	assert.Equal(t, "some header", conf.VHosts["domain1.example.com"].ExportedHeaders["header"])
	assert.Equal(t, "some location", conf.VHosts["domain1.example.com"].LocationRules["location"])
	assert.Equal(t, "other option", conf.VHosts["domain1.example.com"].VhostOptions["vhost"])

	conf.OverrideHost("domain1.example.com", VHost{VhostOptions: map[string]any{"vhost.update": "another option"}, LocationRules: map[string]any{}})
	assert.Equal(t, "another option", conf.VHosts["domain1.example.com"].VhostOptions["vhost.update"])
	// all keys of an overridden property are removed during override
	assert.Nil(t, conf.VHosts["domain1.example.com"].VhostOptions["vhost"])
	assert.Empty(t, conf.VHosts["domain1.example.com"].LocationRules)
}

func TestVHosts_Overwrite(t *testing.T) {
	t.Parallel()

	vhostsA := VHosts{
		"domain1.example.com": {
			ExportedHeaders: map[string]any{
				"Auth-Cn":     "$cn",
				"Auth-Groups": "$groups",
				"Auth-Mail":   "$mail",
				"Auth-User":   "$uid",
			},
			LocationRules: map[string]any{
				"default": "inGroup('role-admin')",
			},
			VhostOptions: map[string]any{
				"vhostHttps":       float64(1),
				"vhostMaintenance": float64(0),
				"vhostPort":        float64(443),
				"vhostType":        "CDA",
			},
		},
		"domain2.example.com": {
			ExportedHeaders: map[string]any{
				"Auth-Cn":     "$cn",
				"Auth-Groups": "$groups",
				"Auth-Mail":   "$mail",
				"Auth-User":   "$uid",
			},
			LocationRules: map[string]any{
				"default": "inGroup('role-user')",
			},
		},
	}
	vhostsB := VHosts{
		"domain2.example.com": {
			LocationRules: map[string]any{
				"default": "inGroup('role-admin')",
			},
			VhostOptions: map[string]any{
				"vhostHttps":       float64(1),
				"vhostMaintenance": float64(0),
				"vhostPort":        float64(443),
				"vhostType":        "CDA",
			},
		},
		"domain3.example.com": {
			LocationRules: map[string]any{},
		},
	}

	vhostOverwritten := vhostsA.Overwrite(vhostsB)
	assert.Equal(
		t,
		VHosts{
			"domain1.example.com": {
				ExportedHeaders: map[string]any{
					"Auth-Cn":     "$cn",
					"Auth-Groups": "$groups",
					"Auth-Mail":   "$mail",
					"Auth-User":   "$uid",
				},
				LocationRules: map[string]any{
					"default": "inGroup('role-admin')",
				},
				VhostOptions: map[string]any{
					"vhostHttps":       float64(1),
					"vhostMaintenance": float64(0),
					"vhostPort":        float64(443),
					"vhostType":        "CDA",
				},
			},
			"domain2.example.com": {
				// NOTE: vhostsA has been overridden by vhostsB
				// ExportedHeaders: map[string]any{
				//	"Auth-Cn":     "$cn",
				//	"Auth-Groups": "$groups",
				//	"Auth-Mail":   "$mail",
				//	"Auth-User":   "$uid",
				// },
				LocationRules: map[string]any{
					"default": "inGroup('role-admin')",
				},
				VhostOptions: map[string]any{
					"vhostHttps":       float64(1),
					"vhostMaintenance": float64(0),
					"vhostPort":        float64(443),
					"vhostType":        "CDA",
				},
			},
			"domain3.example.com": {
				LocationRules: map[string]any{},
			},
		},
		vhostOverwritten,
	)

	// NOTE: vhostA must not be modified
	assert.Equal(t,
		VHosts{
			"domain1.example.com": {
				ExportedHeaders: map[string]any{
					"Auth-Cn":     "$cn",
					"Auth-Groups": "$groups",
					"Auth-Mail":   "$mail",
					"Auth-User":   "$uid",
				},
				LocationRules: map[string]any{
					"default": "inGroup('role-admin')",
				},
				VhostOptions: map[string]any{
					"vhostHttps":       float64(1),
					"vhostMaintenance": float64(0),
					"vhostPort":        float64(443),
					"vhostType":        "CDA",
				},
			},
			"domain2.example.com": {
				ExportedHeaders: map[string]any{
					"Auth-Cn":     "$cn",
					"Auth-Groups": "$groups",
					"Auth-Mail":   "$mail",
					"Auth-User":   "$uid",
				},
				LocationRules: map[string]any{
					"default": "inGroup('role-user')",
				},
			},
		},
		vhostsA,
	)
}
