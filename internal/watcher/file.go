package watcher

import (
	"os"
	"sync"
	"time"

	"github.com/fsnotify/fsnotify"
)

type (
	File struct {
		content []byte
		lastErr error

		fswatcher *fsnotify.Watcher
		sig       chan time.Time
		mut       sync.RWMutex
	}
)

// NewFile returns a new file watcher for the given path.
func NewFile(path string) (*File, error) {
	watcher, err := fsnotify.NewWatcher()
	if err != nil {
		return nil, err
	}

	file := &File{fswatcher: watcher, sig: make(chan time.Time)}
	go file.watch(path)

	{ // Thread-safe block
		file.mut.Lock()
		file.content, err = os.ReadFile(path)
		file.lastErr = err
		file.mut.Unlock()
	}

	err = watcher.Add(path)
	if err != nil {
		return nil, err
	}

	return file, err
}

// Content reads the named file and returns the contents of the named file.
func (file *File) Content() ([]byte, error) {
	file.mut.RLock()
	defer file.mut.RUnlock()

	cpy := make([]byte, len(file.content))
	copy(cpy, file.content)
	return file.content, file.lastErr
}

// Changed returns a channel that delivers a signal when the file is changed.
func (file *File) Changed() <-chan time.Time { return file.sig }

// Close closes all internal channels and stop to watch the named file.
func (file *File) Close() error {
	err := file.fswatcher.Close()
	return err
}

// watch watches if the named file has changed and updates the internal copy if so.
//
//nolint:cyclop // the cyclomatic complexity exceeding 10 is due to the thread-safe block.
func (file *File) watch(path string) {
	defer func() { close(file.sig) }()
	for {
		select {
		case event, open := <-file.fswatcher.Events:
			if !open {
				return
			}

			if !(event.Has(fsnotify.Create) ||
				event.Has(fsnotify.Rename) ||
				event.Has(fsnotify.Write) ||
				event.Has(fsnotify.Remove)) {
				// ignore if nothing changed
				continue
			}

			content, err := os.ReadFile(path)

			{ // Thread-safe block
				file.mut.Lock()
				if err != nil {
					file.lastErr = err
				} else {
					file.content = content
				}
				file.mut.Unlock()
			}
			file.sig <- time.Now()

		case err, open := <-file.fswatcher.Errors:
			if !open {
				return
			}

			{ // Thread-safe block
				file.mut.Lock()
				file.lastErr = err
				file.mut.Unlock()
			}
		}
	}
}
