package watcher_test

import (
	"fmt"
	"io"
	"os"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"github.com/stretchr/testify/suite"
	"gitlab.com/radiofrance/lemonldapng-operator/internal/watcher"
)

func TestNewFile_FileExists(t *testing.T) {
	t.Parallel()

	file, err := os.CreateTemp(t.TempDir(), "")
	require.NoError(t, err)

	_, err = io.WriteString(file, `{}`)
	require.NoError(t, err)

	w, err := watcher.NewFile(file.Name())
	assert.NoError(t, err)
	assert.NotNil(t, w)

	_ = w.Close()
}

func TestNewFile_FileNotFound(t *testing.T) {
	t.Parallel()

	w, err := watcher.NewFile("unknown")
	assert.Error(t, err, "lstat unknown: no such file or directory")
	assert.Nil(t, w)
}

func TestFile_Close(t *testing.T) {
	t.Parallel()

	file, err := os.CreateTemp(t.TempDir(), "")
	require.NoError(t, err)

	_, err = io.WriteString(file, `{}`)
	require.NoError(t, err)

	watcher, err := watcher.NewFile(file.Name())
	assert.NoError(t, err)
	assert.NotNil(t, watcher)

	assert.Falsef(t, hasMessage(watcher.Changed())(), "No message expected on w.Changed(), but get one.")

	err = watcher.Close()
	assert.NoError(t, err)

	select {
	case _, open := <-watcher.Changed():
		if open {
			assert.Fail(t, "w.Changed() is not closed, a message has been caught.")
		}
	case <-time.After(time.Millisecond):
		assert.Fail(t, "w.Changed() is not closed, the channel remains blocking.")
	}
}

type FileTestSuite struct {
	suite.Suite

	file    string
	watcher *watcher.File
}

func (s *FileTestSuite) SetupTest() {
	file, err := os.CreateTemp(s.T().TempDir(), "")
	s.Require().NoError(err)

	_, err = io.WriteString(file, `{}`)
	s.Require().NoError(err)

	err = file.Close()
	s.Require().NoError(err)

	w, err := watcher.NewFile(file.Name())
	s.NoError(err)
	s.NotNil(w)

	s.file = file.Name()
	s.watcher = w
}

func (s *FileTestSuite) TearDownTest() {
	_ = s.watcher.Close()
}

func (s *FileTestSuite) TestFileExists() {
	content, err := s.watcher.Content()
	s.NoError(err)
	s.Equal([]byte(`{}`), content)
}

func (s *FileTestSuite) TestFileNotChanged() {
	s.Never(hasMessage(s.watcher.Changed()), 250*time.Millisecond, time.Millisecond)
}

func (s *FileTestSuite) TestFileChanged() {
	err := os.WriteFile(s.file, []byte("[]"), 0o600)
	s.Require().NoError(err)

	s.Eventually(hasMessage(s.watcher.Changed()), time.Second, time.Millisecond)

	content, err := s.watcher.Content()
	s.NoError(err)
	s.Equal([]byte(`[]`), content)
}

func (s *FileTestSuite) TestFileRemoved() {
	err := os.Remove(s.file)
	s.Require().NoError(err)

	s.Eventually(hasMessage(s.watcher.Changed()), time.Second, time.Millisecond)

	content, err := s.watcher.Content()
	s.Error(err, fmt.Sprintf("open %s: no such file or directory", s.file))
	s.Equal([]byte(`{}`), content)
}

func TestFileTestSuite(t *testing.T) {
	t.Parallel()

	suite.Run(t, new(FileTestSuite))
}

func hasMessage[T any](ch <-chan T) func() bool {
	return func() bool {
		select {
		case _, open := <-ch:
			if !open {
				return false
			}
			return true
		default:
			return false
		}
	}
}
